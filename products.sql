CREATE TABLE products(
    sku varchar(20) not null UNIQUE,
    name varchar(20) not null,
    price int not null,
    attribute varchar(30) not null
);
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('123qwer', 'dvd_1', 1000, '120mb');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('124asdf', 'dvd_2', 1100, '140mb');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('125zxcv', 'potter', 1200, '12kg');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('126ghjk', 'dune', 1300, '100kg');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('127rtyu', 'dvd_3', 1400, '150mb');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('128asdf', 'warhammer', 1500, '40000kg');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('129zxcaq', 'warcraft', 1600, '10000kg');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('130eqxsa', 'furnit_2', 1700, 'Hx42Wx12Lx12');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('131xzasd', 'furnit_1', 1800, 'Hx12Wx10x10');
INSERT INTO `products`(`sku`, `name`, `price`, `attribute`) VALUES ('132zxcad', 'table', 1900, 'Hx10Wx12Lx22');