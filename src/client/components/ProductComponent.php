<?php

namespace app\client\components;

class ProductComponent
{
    public string $sku;
    public string $name;
    public int $price;
    public string $attribute;

    public function __construct($sku, $name, $price, $attribute)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->attribute = $attribute;
    }
    public static function createComponent($sku, $name, $price, $attribute): ProductComponent
    {
        return new ProductComponent($sku, $name, $price, $attribute);
    }
    public function __toString()
    {
        return sprintf(
            '<div class="cell">
        <div class="product">
            <h2>%s</h2>
            <h3>%s</h3>
            <h3>%s$</h3>
            <h3>%s</h3>
        <div class="circle"></div>
    </div></div>',
            $this->sku,
            $this->name,
            $this->price,
            $this->attribute
        );
    }
}
