(function () {
    const select = document.querySelector('#type');
    const save = document.querySelector('.save');
    const form = document.querySelector('form');

    let current = document.querySelector('.show');

    function checkParams(...fields) {
        const errors = [];

        fields.forEach(field => {
            require(field.value) ? pass(field) : errors.push({ field, message: `You must add some additional data for ${field.name}` })
        });

        return errors;
    }
    function pass(field) {
        field.classList.remove('error');
    }
    function require(param) {
        return param.length > 0;
    }
    function errorPop(field, text) {
        field.parentElement.children[2].innerText = text;
        field.parentElement.classList.add('error');
    }
    function getTypeValue(inputs) {
        let value = '';
        if (inputs[0].name === 'size') {
            value = `${inputs[0].value}mb`;
        } else if (inputs[0].name === 'weight') {
            value = `${inputs[0].value}kg`;
        }
        else {

            value = `H${inputs[0].value}W${inputs[1].value}L${inputs[2].value}`;
        }
        return value;
    }
    function showErrors(errors) {
        errors.forEach(({ field, message }) => {
            errorPop(field, message);
        });
    }
    function selectHandler(e) {

        if (current.children[0].classList.contains('error')) {
            current.children[0].classList.remove('error');
        }

        current.classList.remove('show');
        current = document.querySelector(`.${e.target.value}`);
        current.classList.add('show');

    }
    function saveHandler(e) {
        const inputs = current.querySelectorAll('input');
        const errors = checkParams(form.name, form.price, form.sku, ...inputs);
        const typeValue = getTypeValue(inputs)
        form.attribute.value = typeValue;
        errors.length > 0 ? showErrors(errors) : form.submit();
    }
    function inputHandler(e) {
        e.target.parentElement.classList.remove('error');
    }
    select.addEventListener('change', selectHandler);
    save.addEventListener('click', saveHandler);
    document.addEventListener('change', inputHandler);
})()