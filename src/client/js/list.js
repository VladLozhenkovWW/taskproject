(function () {
    const danger = document.querySelector('.danger');
    const form = document.querySelector('.delete');
    function checkHandler(e) {
        if (e.target.classList.contains('product')) {
            e.target.classList.toggle('check');
        }
    }
    function dangerHandler(e) {
        form.checks.value = getAllChecks();
        form.submit();
    }
    function getAllChecks() {
        const checks = document.querySelectorAll('.check');
        const products_ids = [...checks].map(check => check.children[0].innerHTML);
        return products_ids;
    }
    document.addEventListener('click', checkHandler);
    danger.addEventListener('click', dangerHandler);
})()
