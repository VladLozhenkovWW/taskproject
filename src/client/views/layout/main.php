<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/src/client/styles/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Task</title>
</head>

<body>
    {{content}}
    <footer>
        <div class="center container">
            <div class="copy">
                <h3>Junior developer test task</h3>
                <h3>2021 ©</h3>
            </div>
        </div>
    </footer>
    </script>
</body>

</html>