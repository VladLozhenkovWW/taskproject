<nav>
    <div class="container">
        <div class="left">
            <h1>Add Product</h1>
        </div>
        <div class="right">
            <button class="btn save">Save</button>
            <a class="btn" href="/">Cancel</a>
        </div>
    </div>
</nav>
<div class="box center container">
    <form class="col send" action="/product" method="POST">
        <div class="col attributes">
            <div class="group <?php echo isset($error) ? 'error' : '' ?>">
                <label for="sku">SKU:</label>
                <input type="text" name="sku" id="sku">
                <div class="pop">
                    <h3><?php echo isset($error) ? $error : '' ?></h3>
                </div>
            </div>
            <div class="group">
                <label for="name">Name:</label>
                <input type="text" name="name" id="name">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
            <div class="group">
                <label for="price">Price($):</label>
                <input type="number" name="price" id="price">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
            <div class="group">
                <label for="type">Type:</label>
                <select type="select" name="type" id="type">
                    <option value="dvd">DVD-disc</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
            <input type="hidden" name="attribute">
        </div>
    </form>
    <div class="col params">
        <div class="show dvd type">
            <div class="group">
                <label for="size">Size(MB):</label>
                <input type="number" name="size" id="size">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
        </div>
        <div class="book type">
            <div class="group">
                <label for="weight">Weight(Kg):</label>
                <input type="number" name="weight" id="weight">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
        </div>
        <div class="furniture type">
            <div class="group">
                <label for="height">Height(CM):</label>
                <input type="number" name="height" id="height">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
            <div class="group">
                <label for="width">Width(CM):</label>
                <input type="number" name="width" id="width">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
            <div class="group">
                <label for="length">Length(CM):</label>
                <input type="number" name="length" id="length">
                <div class="pop">
                    <h3>Info</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<script defer src="/src/client/js/product.js"></script>