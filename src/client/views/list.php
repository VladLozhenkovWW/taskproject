<?php

use app\client\components\ProductComponent;
?>

<nav>
    <div class="container">
        <div class="left">
            <h1>Product list</h1>
        </div>
        <div class="right">
            <a class='btn' href="/product">Add</a>
            <button class='btn danger'>Mass Delete</button>
        </div>
    </div>
</nav>
<div class="box">
    <div class="grid">
        <?php
        foreach ($products as $product) {
            list($a, $b, $c, $d) =  $product;
            echo ProductComponent::createComponent($a, $b, $c, $d);
        }
        ?>
    </div>
</div>
<form action="/" method="POST" class="delete">
    <input type="hidden" name="checks" value="">
</form>
<script defer src="/src/client/js/list.js">
</script>