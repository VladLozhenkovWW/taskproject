<?php

namespace app\server\models;

use app\server\database\Database;

class Product
{
    const SELECT_ALL = "SELECT * FROM products";
    const INSERT_PRODUCT = "INSERT INTO products(sku, name, price, attribute) VALUES (:sku, :name, :price, :attribute)";
    const DELETE_PRODUCTS = "DELETE FROM products WHERE sku=:id";
    const UNIQUE_SKU = "SELECT sku FROM products WHERE sku=:id";

    public string $sku = '';
    public string $name = '';
    public string $price = '';
    public string $attribute = '';

    public function __construct()
    {
    }
    public function getAll()
    {
        $select = Database::$pdo->prepare(self::SELECT_ALL);
        $select->execute();
        return $select->fetchAll();
    }
    public function insert()
    {
        if ($this->sku) {
            $insert = Database::$pdo->prepare(self::INSERT_PRODUCT);
            $insert->bindParam(':sku', $this->sku);
            $insert->bindParam(':price', $this->price);
            $insert->bindParam(':attribute', $this->attribute);
            $insert->bindParam(':name', $this->name);
            $insert->execute();
        }
    }
    public function deleteProducts(array $products)
    {
        $checks = $products['checks'];
        $ids = explode(',', $checks);
        $delete = Database::$pdo->prepare(self::DELETE_PRODUCTS);

        foreach ($ids as $id) {
            $delete->bindParam(':id', $id);
            $delete->execute();
        }
    }
    public function checkUnique(): bool
    {
        $delete = Database::$pdo->prepare(self::UNIQUE_SKU);
        $delete->bindParam(':id', $this->sku);
        $delete->execute();
        return count($delete->fetchAll()) > 0;
    }
    public function loadData(array $data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }
}
