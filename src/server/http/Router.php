<?php

namespace app\server\http;

use app\server\Application;

class Router
{
    private Request $request;

    private static array $routes;

    public function __construct(Request $request)
    {
        self::$routes = [];

        $this->request = $request;
    }

    public static function get(string $path, string|array $callback): void
    {
        self::$routes['GET'][$path] = $callback;
    }
    public static function post(string $path, string|array $callback): void
    {
        self::$routes['POST'][$path] = $callback;
    }

    public function resolve()
    {
        $callback = $this->getCallback();
        switch (true) {
            case !$callback:
                return View::errorPage();
            case is_string($callback):
                return View::view($callback);
            case is_array($callback):
                return $this->getController($callback);
        };
    }
    private function getCallback()
    {
        $path = $this->request->getPath();
        $method = $this->request->getMethod();
        return self::$routes[$method][$path] ?? false;
    }
    private function getController($callback)
    {
        Application::$app->setController(new $callback[0]());
        $callback[0] = Application::$app->getController();
        return call_user_func($callback, $this->request);
    }
}
