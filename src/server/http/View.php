<?php

namespace app\server\http;

use app\server\Application;

class View
{
    public static function view(string $path, array $props = [])
    {
        $content = self::getContent($path, $props);
        $layout = self::getLayout('main');
        return str_replace('{{content}}', $content, $layout);
    }

    public static function errorPage()
    {
        Response::setStatusCode(404);
        return self::view('_404');
    }
    private static function getLayout($layout)
    {
        ob_start();
        include_once Application::$BASE_DIR . "/src/client/views/layout/$layout.php";
        return ob_get_clean();
    }
    private static function getContent($view, $props)
    {
        foreach ($props as $key => $value) {
            $$key = $value;
        }
        ob_start();
        include_once Application::$BASE_DIR . "/src/client/views/$view.php";
        return ob_get_clean();
    }
}
