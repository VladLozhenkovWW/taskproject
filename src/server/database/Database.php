<?php

namespace app\server\database;

use PDO;

class Database
{

    static PDO $pdo;
    public static $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
    ];

    public function __construct(array $config)
    {
        $dsn = $config['dsn'];
        $username = $config['username'];
        $password = $config['password'];

        self::$pdo = new PDO($dsn, $username, $password, self::$options);
    }
}
