<?php

namespace app\server;

use app\server\controllers\Controller;
use app\server\database\Database;
use app\server\http\Request;
use app\server\http\Response;
use app\server\http\Router;
use app\server\http\View;

class Application
{
    private View $view;
    private Request $request;
    private Response $response;
    private Router $router;
    private Controller $controller;
    public Database $database;
    static public string $BASE_DIR;
    static Application $app;
    public function __construct(array $config, string $BASE_DIR)
    {
        self::$BASE_DIR = $BASE_DIR;
        self::$app = $this;

        $this->view = new View();
        $this->database = new Database($config);
        $this->request = new Request();
        $this->response = new Response();
        $this->router = new Router($this->request);
    }

    public function run(): void
    {
        echo $this->router->resolve();
    }
    public function setController(Controller $controller): void
    {
        $this->controller = $controller;
    }
    public function getController(): Controller
    {
        return $this->controller;
    }
}
