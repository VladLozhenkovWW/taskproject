<?php

namespace app\server\controllers;

use app\server\http\View;

class Controller
{
    public function view(string $path, array $props = [])
    {
        return View::view($path, $props);
    }
}
