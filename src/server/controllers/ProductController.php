<?php

namespace app\server\controllers;

use app\server\http\Request;
use app\server\models\Product;

class ProductController extends Controller
{
    public Product $productModel;

    public function __construct()
    {
        $this->productModel = new Product();
    }

    public function index()
    {
        return $this->view('product', ['uri' => '/product']);
    }
    public function add(Request $request)
    {
        $this->productModel->loadData($request->getBody());
        if ($this->productModel->checkUnique()) {
            return $this->view('product', ["error" => "You must have unique sku"]);
        } else {
            $this->productModel->insert();
            return $this->view('list', ["products" => $this->productModel->getAll()]);
        }
    }
}
