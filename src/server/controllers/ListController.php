<?php

namespace app\server\controllers;

use app\server\http\Request;
use app\server\models\Product;

class ListController extends Controller
{
    public Product $productModel;

    public function __construct()
    {
        $this->productModel = new Product();
    }

    public function index()
    {
        return $this->view('list', ['products' => $this->productModel->getAll()]);
    }
    public function deleteProducts(Request $request)
    {
        $this->productModel->deleteProducts($request->getBody());
        return $this->view('list', ['products' => $this->productModel->getAll()]);
    }
}
