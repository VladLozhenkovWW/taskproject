<?php

use app\server\Application;
use app\server\controllers\ListController;
use app\server\controllers\ProductController;
use app\server\http\Router;


require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$config = [
    'dsn' => $_ENV['DNS_CONNECTION'],
    'username' => $_ENV['DB_USERNAME'],
    'password' => $_ENV['DB_PASSWORD'],
];

$app = new Application($config, __DIR__);

Router::get('/', [ListController::class, 'index']);
Router::post('/', [ListController::class, 'deleteProducts']);

Router::get('/product', [ProductController::class, 'index']);
Router::post('/product', [ProductController::class, 'add']);

$app->run();
